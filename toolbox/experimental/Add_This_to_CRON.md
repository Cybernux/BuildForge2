# Add these to /etc/cron.d



Create and Edit '/etc/cron.d/local-main-debmirror':
```
0 2 * * *       root     /root/BuildForge2/toolbox/experimental/mirror-main > echo "Nightly Backup Successful: $(date)" >> /srv/mirror/nightly-main-mirror-update.log
```

Create and Edit '/etc/cron.d/local-security-debmirror':
```
0 3 * * *       root     /root/BuildForge2/toolbox/experimental/mirror-security > echo "Nightly Backup Successful: $(date)" >> /srv/mirror/nightly-security-mirror-update.log
```

Create and Edit '/etc/cron.d/local-multimedia-debmirror':
```
0 4 * * *       root     /root/BuildForge2/toolbox/experimental/mirror-multimedia > echo "Nightly Backup Successful: $(date)" >> /srv/mirror/nightly-multimedia-mirror-update.log
```
