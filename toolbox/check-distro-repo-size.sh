#!/bin/bash

###############################################################################
#    (C) 2009-2016 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.cybernux.tk
#	 email:    hyperclock(at)cybernux(dot)tk
#	 git:	   https://gitlab.com/Cybernux/BuildForge.git
# 
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build Cybernux Linux® based on Debian.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see docs/gpl-3.0.txt) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################

## –––––––––––––––––––| Get distro information |––––––––––––––––––––– ##
. config

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––| Variables |–––––––––––––––––––––––––– ##
CDRSLOG=${BASEDIR}/repo-size.log

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## –––––––––––––––––––––––| PREPARE LOGGING |–––––––––––––––––––––––– ##
if test -s ${CDRSLOG}
then
test -f ${CDRSLOG}.3.gz && mv ${CDRSLOG}.3.gz ${CDRSLOG}.4.gz
test -f ${CDRSLOG}.2.gz && mv ${CDRSLOG}.2.gz ${CDRSLOG}.3.gz
test -f ${CDRSLOG}.1.gz && mv ${CDRSLOG}.1.gz ${CDRSLOG}.2.gz
test -f ${CDRSLOG}.0 && mv ${CDRSLOG}.0 ${CDRSLOG}.1 && gzip ${CDRSLOG}.1
mv ${CDRSLOG} ${CDRSLOG}.0
cp /dev/null ${CDRSLOG}
chmod 640 ${CDRSLOG}
fi
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##


## ––––––––––––––––––––––| Fix Owner & Perm. |––––––––––––––––––––––– ##
echo "\n*** Fixing ownership ***\n" 2>&1 | tee -a ${CDRSLOG}
find ${REPODST} -type d -o -type f -exec chown ${OWNER}:${PUBLIC} '{}' \; \
2>&1 | tee -a ${CDRSLOG}

echo "\n*** Fixing permissions ***\n" 2>&1 | tee -a ${CDRSLOG}
find ${REPODST} -type d -o -type f -exec chmod u+rw,g+r,o+r-w '{}' \; \
2>&1 | tee -a ${CDRSLOG}

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––| Get Mirror Size |–––––––––––––––––––––––– ##
echo "\n*** Mirror size ***\n" 2>&1 | tee -a ${CDRSLOG}
du -hs ${REPODST} 2>&1 | tee -a ${CDRSLOG}

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––| Record the current date/time |––––––––––––––––– ##
date 2>&1 | tee -a ${CDRSLOG}

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

