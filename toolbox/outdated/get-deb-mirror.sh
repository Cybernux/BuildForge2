#!/bin/bash

###############################################################################
#    (C) 2009-2016 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.cybernux.tk
#	 email:    hyperclock(at)cybernux(dot)tk
#	 git:	   https://gitlab.com/Cybernux/BuildForge.git
# 
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build Cybernux Linux® based on Debian.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see docs/gpl-3.0.txt) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################


## =======================
## Make your changes below
## =======================
ARCHITECTURES="i386,amd64"
MIRRORDIR='/deb-mirror'

SECTIONS="main,main/debian-installer,contrib,non-free"

MAIN_DISTS="jessie,jessie-updates,jessie-backports"
MAIN_REMOTE_MIRROR="ftp.de.debian.org"
MAIN_LOCAL_MIRROR="${MIRRORDIR}/debian"

SEC_DISTS="jessie/updates"
SEC_REMOTE_MIRROR="security.debian.org"
SEC_LOCAL_MIRROR="${MIRRORDIR}/debian-sec"

MM_SECTIONS="main,non-free"
MM_DISTS="jessie"
MM_REMOTE_MIRROR="www.deb-multimedia.org"
MM_LOCAL_MIRROR="${MIRRORDIR}/debian-mm"

## ====================================
## put OPT_SOURCES as "--source" or 
## leave blank to have sources excluded
## ====================================
OPT_SOURCES="--source"

###############################################################
### Don't change below, unless you know what you are doing. ###
###############################################################

## ––––––––––––––––––––––| LOGGING VARIABLES |––––––––––––––––––––––– ##
OWNER="www-data"
PUBLIC="www-data"
GDMLOG="${MIRRORDIR}/get-deb-mirror.log"

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––| PREPARE LOGGING |–––––––––––––––––––––––– ##
if test -s $GDMLOG
then
test -f $GDMLOG.3.gz && mv $GDMLOG.3.gz $GDMLOG.4.gz
test -f $GDMLOG.2.gz && mv $GDMLOG.2.gz $GDMLOG.3.gz
test -f $GDMLOG.1.gz && mv $GDMLOG.1.gz $GDMLOG.2.gz
test -f $GDMLOG.0 && mv $GDMLOG.0 $GDMLOG.1 && gzip $GDMLOG.1
mv $GDMLOG $GDMLOG.0
cp /dev/null $GDMLOG
chmod 640 $GDMLOG
fi
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## ––––––––––––––––| Record the current date/time |–––––––––––––––––– ##
date 2>&1 | tee -a $GDMLOG
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––| BEGIN MAIN MIRROR |–––––––––––––––––––––––––– ##
LOCK="${MAIN_LOCAL_MIRROR}/mirror.lock"


if [ -f "${LOCK}" ]; then
        echo "ERUNNING: \"`basename ${0}`\" is already running."
        exit 1
else
        trap "test -f ${LOCK} && rm -f ${LOCK}; exit 0" 0 2 15

        touch ${LOCK}

        debmirror \
        --arch="${ARCHITECTURES}" \
        --section="${SECTIONS}" \
        --method=http \
        --root=":debian" \
        ${OPT_SOURCES} \
        --host="${MAIN_REMOTE_MIRROR}" \
        --no-check-gpg \
        --ignore-missing-release \
        --dist="${MAIN_DISTS}" \
        --diff=none \
        --i18n \
        --omit-suite-symlinks \
        --progress "${MAIN_LOCAL_MIRROR}"
        2>&1 | tee -a $GDMLOG


fi

# Extract the lists so awk can handle them
find ${MAIN_LOCAL_MIRROR}/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find ${MAIN_LOCAL_MIRROR}/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## ––––––––––––––––––––––| Fix Owner & Perm. |––––––––––––––––––––––– ##
echo "\n*** Fixing ownership ***\n" 2>&1 | tee -a $GDMLOG
find ${MAIN_LOCAL_MIRROR} -type d -o -type f -exec chown $OWNER:$PUBLIC '{}' \; \
2>&1 | tee -a $GDMLOG

echo "\n*** Fixing permissions ***\n" 2>&1 | tee -a $DEBMLOG
find ${MAIN_LOCAL_MIRROR} -type d -o -type f -exec chmod u+rw,g+r,o+r-w '{}' \; \
2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––| Get Mirror Size |–––––––––––––––––––––––– ##
echo "\n*** Mirror size ***\n" 2>&1 | tee -a $GDMLOG
du -hs ${MAIN_LOCAL_MIRROR} 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––| Record the current date/time |––––––––––––––––– ##
date 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## ––––––––––––––––––––| BEGIN SECURITY MIRROR |––––––––––––––––––––– ##
SECLOCK="${SEC_LOCAL_MIRROR}/mirror.lock"


if [ -f "${SECLOCK}" ]; then
        echo "ERUNNING: \"`basename ${0}`\" is already running."
        exit 1
else
        trap "test -f ${SECLOCK} && rm -f ${SECLOCK}; exit 0" 0 2 15

        touch ${SECLOCK}

        debmirror 
        --arch="${ARCHITECTURES}" \
        --section="${SECTIONS}" \
        --method=http \
        --root="/" \
        ${OPT_SOURCES} \
        --host="${SEC_REMOTE_MIRROR}" \
        --no-check-gpg \
        --ignore-missing-release \
        --dist="${SEC_DISTS}" \
        --diff=none \
        --i18n \
        --omit-suite-symlinks \
        --progress "${SEC_LOCAL_MIRROR}"
        2>&1 | tee -a $GDMLOG
        

fi

# Extract the lists so awk can handle them
find ${SEC_LOCAL_MIRROR}/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find ${SEC_LOCAL_MIRROR}/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## ––––––––––––––––––––––| Fix Owner & Perm. |––––––––––––––––––––––– ##
echo "\n*** Fixing ownership ***\n" 2>&1 | tee -a $GDMLOG
find ${SEC_LOCAL_MIRROR} -type d -o -type f -exec chown $OWNER:$PUBLIC '{}' \; \
2>&1 | tee -a $GDMLOG

echo "\n*** Fixing permissions ***\n" 2>&1 | tee -a $DEBMLOG
find ${SEC_LOCAL_MIRROR} -type d -o -type f -exec chmod u+rw,g+r,o+r-w '{}' \; \
2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––| Get Mirror Size |–––––––––––––––––––––––– ##
echo "\n*** Mirror size ***\n" 2>&1 | tee -a $GDMLOG
du -hs ${SEC_LOCAL_MIRROR} 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––| Record the current date/time |––––––––––––––––– ##
date 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## –––––––––––––––––––| BEGIN MULTIMEDIA MIRROR |–––––––––––––––––––– ##
MMLOCK="${MM_LOCAL_MIRROR}/mirror.lock"


if [ -f "${MMLOCK}" ]; then
        echo "ERUNNING: \"`basename ${0}`\" is already running."
        exit 1
else
        trap "test -f ${MMLOCK} && rm -f ${MMLOCK}; exit 0" 0 2 15

        touch ${MMLOCK}

        debmirror 
        --arch="${ARCHITECTURES}" \
        --section="${MM_SECTIONS}" \
        --method=http \
        --root="/" \
        ${OPT_SOURCES} \
        --host="${MM_REMOTE_MIRROR}" \
        --no-check-gpg \
        --ignore-missing-release \
        --dist="${MM_DISTS}" \
        --diff=none \
        --i18n \
        --omit-suite-symlinks \
        --progress "${MM_LOCAL_MIRROR}"
        2>&1 | tee -a $GDMLOG


fi

# Extract the lists so awk can handle them
find ${MM_LOCAL_MIRROR}/dists/ -name Packages.bz2 -exec bunzip2 -fk {} \;
find ${MM_LOCAL_MIRROR}/dists/ -name Sources.bz2 -exec bunzip2 -fk {} \;

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##

## ––––––––––––––––––––––| Fix Owner & Perm. |––––––––––––––––––––––– ##
echo "\n*** Fixing ownership ***\n" 2>&1 | tee -a $GDMLOG
find ${MM_LOCAL_MIRROR} -type d -o -type f -exec chown $OWNER:$PUBLIC '{}' \; \
2>&1 | tee -a $GDMLOG

echo "\n*** Fixing permissions ***\n" 2>&1 | tee -a $DEBMLOG
find ${MM_LOCAL_MIRROR} -type d -o -type f -exec chmod u+rw,g+r,o+r-w '{}' \; \
2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––––––––| Get Mirror Size |–––––––––––––––––––––––– ##
echo "\n*** Mirror size ***\n" 2>&1 | tee -a $GDMLOG
du -hs ${MM_LOCAL_MIRROR} 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##
## –––––––––––––––––| Record the current date/time |––––––––––––––––– ##
date 2>&1 | tee -a $GDMLOG

## –––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– ##







