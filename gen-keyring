#!/bin/bash

###############################################################################
#    (C) 2013 hyperclock (aka Jimmy M. Coleman)
#	 website:  www.hubshark.com
#	 email:    hyperclock(at)hubshark(dot)com
###############################################################################
###############################################################################
#    BuildForge - Scripts designed to build the HubShark OS, utilizing 
#	 Debian(and/or Devuan) GNU/Linux as a base-.
#
#    BuildForge is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BuildForge is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy (see COPYING) of the GNU General 
#    Public License along with BuildForge.  If not, see 
#    <http://www.gnu.org/licenses/>.
#
################################################################################
################################ INFO ##########################################
#
#	Derived from the scripts used by the gNewSense Project.
#	See the original scripts at http://gnewsense.org
#
#	Original (C) 2006 - 2009  Brian Brazil
#
#################################################################################


. config

rm -rf $WORKINGDIR/hubshark-repo-keyring
mkdir -p $WORKINGDIR/hubshark-repo-keyring
cd $WORKINGDIR/hubshark-repo-keyring

mkdir -p hubshark-repo-keyring/debian
cd hubshark-repo-keyring


mkdir keyrings

cp BuildForge2/images/hubshark-repo-signing.key.gpg keyrings/

VERSION="0.0.1"


# find builder -name .bzr -print0 | xargs -0 rm -rf
EDITOR=true dch --create --package hubshark-repo-keyring -v $VERSION-$KEYRING_VERSION "Automatically generated"
sed -i "/XXXXXX/d;s/UNRELEASED/$RELEASE/" debian/changelog

cat > debian/control <<EOF
Source: hubshark-repo-keyring
Section: misc
Priority: extra
Maintainer: $MAINTINFO
Bugs: mailto:hyperclock@hubshark.com
Homepage: https://www.hubshark.com
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9)

Package: hubshark-repo-keyring
Architecture: all
Multi-Arch: foreign
Depends: gpgv, ${misc:Depends}
Recommends: gnupg
Description: GnuPG archive key of the HubShark repository
 The HubShark repository digitally signs its Release files. This
 package contains the repository key used for that.
EOF

cat > debian/rules <<EOF
#!/usr/bin/make -f

%:
	dh $@

override_dh_install:
# gpg --export 65558117 > keyrings/deb-multimedia-keyring.gpg
	dh_install keyrings/*.gpg etc/apt/trusted.gpg.d

EOF
chmod 755 debian/rules

cat > debian/postinstall <<EOF
#!/bin/sh

set -e

if [ "$1" = 'configure' -a -n "$2" ]; then
	# remove keys from the trusted.gpg file as they are now shipped in fragment files in trusted.gpg.d
	if dpkg --compare-versions "$2" 'lt' "2014.2" && which gpg > /dev/null && which apt-key > /dev/null; then
		TRUSTEDFILE='/etc/apt/trusted.gpg'
		eval $(apt-config shell TRUSTEDFILE Apt::GPGV::TrustedKeyring)
		eval $(apt-config shell TRUSTEDFILE Dir::Etc::Trusted/f)
		if [ -e "$TRUSTEDFILE" ]; then
			for KEY in 1F41B907 65558117; do
				apt-key --keyring "$TRUSTEDFILE" del $KEY > /dev/null 2>&1 || :
			done
		fi
	fi
fi

#DEBHELPER#
EOF

dpkg-buildpackage $DPKGOPTS
cd ..
reprepro -Vb $REPODST include $RELEASE hubshark-repo-keyring*.changes

