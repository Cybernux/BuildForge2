# IMPORTANT


## packages-to-remove

This was the file I had for another project. This may get added. Just seems like extra bloat on the repo, to me.

More will added. These will be then removed, with replacement, from our repository.



## fork-from-debian.txt

This file was taken from the Devuan project. Some of these packages were changed due to branding.

Some were changed to keep a clean SysV. Some were maybe both. 

We need to check that and put all *branding* only related packages into the **BRANDING** category.

We need another category, **SysV**. This is where the packages that were changed because of it's SysV relationship.

A third category, **BOTH** could contain the packages that were changed due to fixing problems in both of the above categories.

