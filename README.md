# BuildForge


**BuildForge** is a set of scripts originally designed by Brian Brazil, named _builder_ and used by the gNewSense project.
The gNewSense project removed "_non-free_" things from many parts of _Ubuntu_, which serverd as a base for thier Operating System. The gNewSense project has abandoned the _builder scripts_ in favour of thier new _debderiver_.
 
BuildForge is based on the "builder" rev.430 and reworked, modded, hacked - to fit the needs of the 
Cybernux Project. Debian is the _Upstream Base distribution. Not only are we not removing the stuff that gNewSense did, but we may add more to it.



## License(s)


LICENSE DETAILS:	see [LICENSE](LICENSE).

GNEWSENSE BUILDER DETAILS:	see [the original README](README.original).



## ISSUES, BUGS & Co.

Issues, bugs, sugesstion, etc. can be (should be) places in our [ISSUES](../../issues). 

We will also try to keep a _MILESTONE_ setup so you can keep updated as to where devlopment is at.



## Documentation

As far as possible we will keep an updated [WIKI](../../wikis/home) on the BuildForge scripts. There may be some documentation in the _docs_ folder, but they may be somewhat old.

Getting started information is also in the [WIKI](../../wikis/StartHere)



----

	-- hyperclock, updated on 18.03.2021
	 